<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Batch Allocation</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<style>
.error
{
color:red;
}
</style>
</head>
<body>
<div class="topnav">
		<a class="active" href="#">Academy Portal</a>
		
		
		<div class="topnav-right">
			<a href="displayUsers">View Batches</a>
			<a href="index.jsp"><i class="fa fa-fw fa-home"></i>Home</a>
		</div>
	</div>
	<br />
	<br />
	<br /><br />
	<br />
	<div align="center">
	<form:form action="saveBatch" modelAttribute="batch">
			<table border="0">
			<tr>
				     
					<td colspan="2" align="center"><h2 class="heading1">Batch Allocation </h2></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><p class="heading3">Fill the Batch Details</p></td>
				</tr>

                <tr>
                <td><form:label path="batchName" cssClass="name">Batch Name</form:label><br/></td>
                <td><form:input path="batchName" class="form-control"/></td>
                <td><form:errors cssClass="error" path="batchName"/></td>
                </tr>
                
                <tr>
                <td><form:label path="trainerName" cssClass="name">Batch trainerName</form:label><br/></td>
                <td><form:input path="trainerName" class="form-control"/></td>
                <td><form:errors cssClass="error" path="trainerName"/></td>
                </tr>
                
                <tr>
                <td><form:label path="noOfTrainees" cssClass="name">Batch noOfTrainees</form:label><br/></td>
                <td><form:input path="noOfTrainees" class="form-control"/></td>
                <td><form:errors cssClass="error" path="noOfTrainees"/></td>
                </tr>
                
                <tr>
                <td><form:label path="domain" cssClass="name">Domain</form:label><br/></td>
                <td><form:select path="domain">
              	<form:option value="-" label="--Select Domain--"/>
				<c:forEach var="course" items="${course}">
				<form:option value="${course.courseName} - ${course.courseDuration}"></form:option>
				</c:forEach>
       
                </form:select></td>
                <td><form:errors cssClass="error" path="domain"/></td>
                </tr>
                
                <tr>
                <td Class="name">batchStartDate</td>
                <td><form:input type="date" path= "batchStartDate"/></td>
                <td><form:errors cssClass="error" path="batchStartDate"/></td>
                </tr>
                
                <tr >
                <td Class="name">batchEndDate:</td>
                <td><form:input type="date" path= "batchEndDate"/></td>
                <td><form:errors cssClass="error" path="batchEndDate"/></td>
                </tr>
                
                <tr>
                    <td colspan="2" align="center"><input type="submit" value="Submit" class="btn btn-primary"/></td>
                </tr>
                



			</table>

		</form:form>
	</div>
</body>
</html>
<%-- <form:form action="saveBatch" modelAttribute="batch" style="max-width: 450px; margin:0 auto;">

<div class="form-group">
<form:label path="batchName">Batch Name</form:label><br/>
<form:input path="batchName" class="form-control"/><form:errors cssClass="error" path="batchName"/><br/>
</div>
<div class="form-group">
<form:label path="trainerName">Batch trainerName</form:label><br/>
<form:input path="trainerName" class="form-control"/><form:errors cssClass="error" path="trainerName"/><br/>
</div>
<div class="form-group">
<form:label path="noOfTrainees">Batch noOfTrainees</form:label><br/>
<form:input path="noOfTrainees" class="form-control"/><form:errors cssClass="error" path="noOfTrainees"/><br/>
</div>
<div class="form-group">
<form:label path="domain">domain</form:label><br/>
Domain:<form:select path="domain">
    <form:option value="-" label="--Select Domain--"/>
    <form:option value="java" label="Java"/>
    <form:option value="linux" label="Linux"/>
    <form:option value="Dotnet" label="DotNet"/>
    <form:option value="python" label="Python"/>
    <form:option value="Aws" label="Aws"/> 
    </form:select><br/>
</div>
<div class="form-group">
batchStartDate:<form:input type="date" path= "batchStartDate"/><form:errors cssClass="error" path="batchStartDate"/>
</div>
<div class="form-group">
batchEndDate:<form:input type="date" path= "batchEndDate"/><form:errors cssClass="error" path="batchEndDate"/>
</div>
<input type="submit" value="Submit"/>
</form:form> --%>




