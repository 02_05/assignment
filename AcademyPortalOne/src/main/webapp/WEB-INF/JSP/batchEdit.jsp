<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Batch Details</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<style type="text/css">
.topnav {
  overflow: hidden;
  background-color: #3C4551;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
  font-weight: bold;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #E15050;
  color: white;
}
.table1{
}
</style>
</head>
<body>
<!-- <h1>Edit Batch</h1> -->
<div class="topnav">
		<a class="active" href="#">Academy Portal</a>
		
		
		<div class="topnav-right">
			<a href="#">Batch Updation</a>
			<a href="index.jsp"><i class="fa fa-fw fa-home"></i>Home</a>
		</div>
	</div>
	
	<br><br><br><br><br><br>
	<h1 style="text-align:center">Edit Batch</h1>
	<br/>
<form:form action="/AcademyPortal/batchSave">
<table align="center">
<form:hidden path="batchId" />
<tr>
<td><form:label path="batchName">Batch Name</form:label></td>
<td><form:input path="batchName"/></td>
</tr>
<tr>
<td><form:label path="trainerName">Trainer Name</form:label></td>
<td><form:input path="trainerName"/></td>
</tr>
<tr>
<td><form:label path="noOfTrainees">NoOfTrainees</form:label></td>
<td><form:input path="noOfTrainees"/></td>
</tr>
<tr>
<td><form:label path="domain">Domain</form:label></td>
<td><form:select path="domain">
              	<form:option path="domain" value="${command.domain}"/>
				<c:forEach var="course" items="${course}">
				<form:option value="${course.courseName} - ${course.courseDuration}"></form:option>
				</c:forEach>
       </form:select></td>
 </tr>
 <tr>
<td><form:label path="batchStartDate">BatchStartDate</form:label></td>
<td><form:input type="date" path= "batchStartDate"/></td>
</tr>
<tr>
<td><form:label path="batchEndDate">BatchEndDate</form:label></td>
<td><form:input type="date" path= "batchEndDate"/></td>
</tr>
<tr></tr>
<tr>
<td></td>
<td><input type="submit" value="Submit" class="btn btn-primary"></td>
</tr>
</table>
</form:form>
</body>
</html>



 