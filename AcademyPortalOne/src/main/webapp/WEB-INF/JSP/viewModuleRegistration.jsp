<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ViewModuleDetails</title>
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<link rel="stylesheet" href=
"https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" />
 <script type="text/javascript" 
     src="https://code.jquery.com/jquery-3.5.1.js">
     </script>
  
     <script type="text/javascript" src=
"https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js">
     </script>
<script>
        $(document).ready(function() {
            $('#tableID').DataTable({ });
        });
    </script>
<style type="text/css">
h2{
text-align: center;
}
</style>
</head>
<body>
<div class="topnav">
		<a class="active" href="#">Academy Portal</a>
		
		
		<div class="topnav-right">
		<a href="module">Module Registration</a>
			<a href="moduleUpdate">Module Updation</a>
		</div>
	</div>
	<br />
	<h2>Course Module</h2>
<table id="tableID" class="display" style="width:100%">
<thead>
<tr class="thcolor"><th>Course Id</th><th>Course Name</th><th>Course Duration</th></tr>
</thead>
<tbody>
<c:forEach var="user" items="${listOfUsers}">  
   <tr class="tdcolor">
<td>${user.courseId}</td>
<td>${user.courseName}</td>
<td>${user.courseDuration}</td>
</tr> 
</c:forEach>
</tbody>
</table> 
</body>
</html>

