package com.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Batch;
import com.model.Report;


@Repository
public class ReportDaoImpl implements ReportDaoIntf{
	@Autowired
	SessionFactory sessionFactory;
	public List<Batch> getBatchId() {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from batch_tabl where facultycreditpoint is null").addEntity(Batch.class);
		List<Batch> list=query.list();
		//List<Batch> list = sessionFactory.openSession().createQuery("from Batch").list();
		return list;
	}

	public void calculateCredit(Report report,int val) {
		int point;
		int value=0;
		if(report.getStatusOfReportSubmission().equals("yes")) {
			value=5;
		}
		Batch batch=new Batch();
		point=(report.getRating()+report.getNoOfWorkingDays()+value)/3;
		report.setFacultyCreditPoint(point);
		System.out.println(batch.getBatchId()+" "+report.getRating()+" "+report.getNoOfWorkingDays()+" "+report.getStatusOfReportSubmission()+" "+report.getFacultyCreditPoint());
		//sessionFactory.openSession().createSQLQuery(" batch_tbles(rating,noOfWorkingDays,statusOfReportSubmission,facultyCreditPoint) values("+report.getRating()+","+report.getNoOfWorkingDays()+",'"+report.getStatusOfReportSubmission()+"',"+report.getFacultyCreditPoint()+")").executeUpdate();
		SQLQuery query=sessionFactory.openSession().createSQLQuery("update batch_tabl set rating=:rating,noOfWorkingDays=:noOfDays,statusOfReportSubmission=:status,facultyCreditPoint=:point where batchId=:id").addEntity(Report.class);
		query.setParameter("rating",report.getRating());
		query.setParameter("noOfDays",report.getNoOfWorkingDays());
		query.setParameter("status",report.getStatusOfReportSubmission());
		query.setParameter("point",report.getFacultyCreditPoint());
		query.setParameter("id",val);
		query.executeUpdate();
		
		//sessionFactory.openSession().save(report);
		/*float point;
		int val=0;
		Query query=sessionFactory.openSession().createSQLQuery("update report_tbl set facultyCreditPoint=:point where batchId=:id");
		query.setParameter("id",report.getBatchId());
		if(report.getRating()==5 && report.getStatusOfReportSubmission().equals("yes") ) {
			report.setFacultyCreditPoint(10);
		}else if(report.getRating()==5 && report.getStatusOfReportSubmission().equals("yes"))
		if(report.getStatusOfReportSubmission().equals("yes")) {
			val=5;
		}
		point=(report.getRating()+report.getNoOfWorkingDays()+val)/3;
		query.setParameter("point",point);
		query.executeUpdate();*/
	}

	public List<Batch> getBatchDetails() {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from batch_tabl").addEntity(Batch.class);
		List<Batch> list=query.list();
		//List<Batch> list = sessionFactory.openSession().createQuery("from Batch").list();
		return list;
	}

}
