package com.dao;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Batch;
import com.model.ModuleRegistration;
import com.model.Registration;

 

@Repository

public class BatchDaoImpl implements BatchDaoIntf {
	//connection using hibernate
    @Autowired
    SessionFactory sessionFactory;
    //insert
    public void saveBatch(Batch batch) {
    	SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from faculty_tabl where firstName=:name").addEntity(Registration.class);
    	query.setParameter("name",batch.getTrainerName());
    	Registration register=(Registration) query.uniqueResult();
    	sessionFactory.openSession().save(batch);
    	 String str =batch.getDomain();
         String[] arr = str.split("-");
         System.out.println(arr[0]);
        SQLQuery query2=sessionFactory.openSession().createSQLQuery("update batch_tabl set domain=:course where batchId=:id").addEntity(Batch.class);
     	query2.setParameter("course",arr[0]);
        query2.setParameter("id",batch.getBatchId());
        query2.executeUpdate();
    	SQLQuery query1=sessionFactory.openSession().createSQLQuery("update batch_tabl set register_associateId=:id where trainerName=:name").addEntity(Batch.class);
    	query1.setParameter("id",register.getAssociateId());
    	query1.setParameter("name",batch.getTrainerName());
    	query1.executeUpdate();
    }
    //fetch
    public List<Batch> getUsers()
    {
        List<Batch> ls=sessionFactory.openSession().createQuery("from Batch").list();
        return ls;
    }
	
	public Batch getDetails(int id) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from batch_tabl where batchId=:id").addEntity(Batch.class);
        query.setParameter("id",id);
        Batch batch=(Batch) query.uniqueResult();
        return batch;
	}

	public void edit(Batch batch) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("update batch_tabl set BatchName=:name,trainerName=:tname,noOfTrainees=:trainers,domain=:domain,batchStartDate=:sbatch,batchEndDate=:ebatch where batchId=:id").addEntity(Batch.class);
        query.setParameter("name",batch.getBatchName());
                query.setParameter("tname",batch.getTrainerName());
                query.setParameter("trainers",batch.getNoOfTrainees());
                String str =batch.getDomain();
                String[] arr = str.split("-");
                query.setParameter("domain",arr[0]);
                query.setParameter("sbatch",batch.getBatchStartDate());
                query.setParameter("ebatch",batch.getBatchEndDate());
                query.setParameter("id",batch.getBatchId());
                query.executeUpdate();
	}
	
	public void delete(int id) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("delete from batch_tabl where batchId=:id").addEntity(Batch.class);
        query.setParameter("id",id);
        query.executeUpdate();
	}
	
	public List<ModuleRegistration> getCourses() {
		List<ModuleRegistration> list=sessionFactory.openSession().createQuery("from ModuleRegistration m").list();
		return list;
	}

}
