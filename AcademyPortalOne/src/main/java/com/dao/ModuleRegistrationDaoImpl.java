package com.dao;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.ModuleRegistration;

@Repository
public class ModuleRegistrationDaoImpl implements ModuleRegistrationDaoIntf {
	@Autowired
	SessionFactory sessionFactory;

	public void saveBatch(ModuleRegistration moduleregistration) {
		sessionFactory.openSession().save(moduleregistration);
		
	}

	public List<ModuleRegistration> viewDetails() {
		
		List<ModuleRegistration> ls=sessionFactory.openSession().createQuery("from ModuleRegistration").list();
        return ls;
	}

	@Override
	public ModuleRegistration getDetails(int id) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from moduleregistration where courseId=:id").addEntity(ModuleRegistration.class);
        query.setParameter("id",id);
        ModuleRegistration moduleregistration=(ModuleRegistration) query.uniqueResult();
        return moduleregistration;
		
	}

	@Override
	public void edit(ModuleRegistration moduleregistration) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("update moduleregistration set courseName=:name,courseDuration=:duration where courseId=:id").addEntity(ModuleRegistration.class);
        query.setParameter("name",moduleregistration.getCourseName());
                query.setParameter("duration",moduleregistration.getCourseDuration());
                query.setParameter("id",moduleregistration.getCourseId());
                query.executeUpdate();
	}

	@Override
	public void delete(int id) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("delete from moduleregistration where courseId=:id").addEntity(ModuleRegistration.class);
        query.setParameter("id",id);
        query.executeUpdate();
		
	}
}
