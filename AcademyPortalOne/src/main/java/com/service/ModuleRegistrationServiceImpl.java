package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.ModuleRegistrationDaoIntf;
import com.model.ModuleRegistration;
@Service
@Transactional
public class ModuleRegistrationServiceImpl implements ModuleRegistrationServiceIntf{
	 @Autowired
	 ModuleRegistrationDaoIntf moduleregistrationdaointf;

	

	
	public void saveModuleRegistration(ModuleRegistration moduleregistration) {
		moduleregistrationdaointf.saveBatch(moduleregistration);
		
	}
	
	public List<ModuleRegistration > viewDetails() {
		List<ModuleRegistration> ls=moduleregistrationdaointf.viewDetails();
        return ls;
	}

	@Override
	public ModuleRegistration getDetails(int id) {
		ModuleRegistration moduleregistration=moduleregistrationdaointf.getDetails(id);
		return moduleregistration;
		
	}

	@Override
	public void edit(ModuleRegistration moduleregistration) {
		moduleregistrationdaointf.edit(moduleregistration);
		
	}

	@Override
	public void delete(int id) {
		moduleregistrationdaointf.delete(id);
		
	}
}
