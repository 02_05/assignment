//In the previous program remove the try{}catch(){} block surrounding the sleep method and 
//try to execute the code. What is your observation? 

/*Observation
-----------
	without using try and catch we get Interrupted Exception (Checked Exception) at compile time*/
package com.dayseven;

import java.time.LocalTime;

class Thread2 extends Thread{
	public void run() {
		LocalTime time=LocalTime.now();
		System.out.println(time);
		//try {
			Thread.sleep(10000);
		//} catch (InterruptedException e) {
			//e.printStackTrace();
		//}
		System.out.println(time);
	}
}

public class CurrentThreadDemo {
	public static void main(String[] args) {
		Thread2 t1=new Thread2();
		t1.start();
		t1.setName("MyThread");
		System.out.println(t1.getName());
		
	}
}
