/*Write a program to create a class Number? which implements Runnable. Run method displays the multiples of a number accepted as a parameter. In main create three objects - first object should display the multiples of 2, second should display the multiples of 5 and third should display the multiples of 8. Display appropriate message at the beginning and ending of thread. The main thread should wait for the first object to complete. Display the status of threads before the multiples are displayed and after completing the multiples. 
*/

package com.dayseven;

class Number implements Runnable{
	int number;
	public Number(int n) {
		super();
		this.number=n;
	}
	public void run() {
		for(int i=1;i<=10;i++) {
			System.out.println(this.number*i);
		}
	}
}
public class ClassNumberDemo {
	public static void main(String[] args) throws InterruptedException {
		Number t1=new Number(2);
		Number t2=new Number(5);
		Number t3=new Number(8);
		Thread th1=new Thread(t1);
		Thread th2=new Thread(t2);
		Thread th3=new Thread(t3);
		System.out.println("Thread 1 Started: ");
		th1.start();
		th1.join();
		System.out.println("Thread 1 Completed");
		System.out.println("Thread 2 Started: ");
		th2.start();
		th2.join();
		System.out.println("Thread 2 Completed");
		System.out.println("Thread 3 Started: ");
		th3.start();
		th3.join();
		System.out.println("Thread 3 Completed: ");
	}
}
