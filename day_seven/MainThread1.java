//Rewrite the earlier program so that, now the class DemoThread1 
//instead of implementing from Runnable interface, will now extend from Thread class. 

package com.dayseven;
class DemoThread2 extends Thread{
	public DemoThread2() {
		Thread t1=new Thread();
		t1.start();
		
	}
	public void run() {
		for(int i=1;i<=10;i++) {
			System.out.println("running child Thread in loop : " + i);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
}
public class MainThread1 {
	public static void main(String[] args) {
		System.out.println(1);
		DemoThread2 th1=new DemoThread2();
		DemoThread2 th2=new DemoThread2();
		DemoThread2 th3=new DemoThread2();
		th1.start();
		th2.start();
		th3.start();
	}
}

