/*Write a program to create a class DemoThread1 implementing Runnable interface. 
 *In the constructor, create a new thread and start the thread. 
 *In run() display a message "running child Thread in loop : " 
 *display the value of the counter ranging from 1 to 10. 
 *Within the loop put the thread to sleep for 2 seconds. In main create 3 objects of the DemoTread1 and execute the program
*/
package com.dayseven;

class DemoThread1 implements Runnable{
	public DemoThread1() {
		Thread t1=new Thread();
		t1.start();
		
	}
	public void run() {
		for(int i=1;i<=10;i++) {
			System.out.println("running child Thread in loop : " + i);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
}
public class MainThread {
	public static void main(String[] args) {
		System.out.println(1);
		DemoThread1 th1=new DemoThread1();
		DemoThread1 th2=new DemoThread1();
		DemoThread1 th3=new DemoThread1();
		Thread t1=new Thread(th1);
		Thread t2=new Thread(th2);
		Thread t3=new Thread(th3);
		t1.start();
		t2.start();
		t3.start();
	}
}
