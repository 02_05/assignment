/*Write a program to assign the current thread to t1. 
 *Change the name of the thread to MyThread. 
 *Display the changed name of the thread. 
 *Also it should display the current time. 
 *Put the thread to sleep for 10 seconds and display the time again. 
*/
package com.dayseven;

import java.time.LocalTime;

class Thread1 extends Thread{
	public void run() {
		LocalTime time=LocalTime.now();
		System.out.println(time);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(time);
	}
}

public class CurrentThread {
	public static void main(String[] args) {
		Thread1 t1=new Thread1();
		t1.start();
		t1.setName("MyThread");
		System.out.println(t1.getName());
		
	}
}

