package com.dayfour;

import java.util.Scanner;

public class MainClass {
	public static void main(String[] args) {
		int n;
		MyCalculator cal=new MyCalculator();
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number: ");
		n=sc.nextInt();
		System.out.println(cal.divisor_sum(n));
		sc.close();
	}
}
