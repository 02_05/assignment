package com.dayfour;

class A1 {
	private int a = 100;
	public void setA( int value) {
	a = value;
	}
	public int getA() {
	return a;
	}
} //class A

public class OopExercises4 {
	public static void main(String[] args) { 
		A1 objA = new A1(); 
		System.out.println("in main(): "); 
		System.out.println("objA.a = "+objA.getA()); 
		objA.setA(222); 
		} 
}
