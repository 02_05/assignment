package com.dayfour;

public abstract class Shape {
	protected String name;
	Shape(String name){
		this.name=name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	abstract float calculateArea();
}
class Circle extends Shape{
	private int radius;
	Circle(String name,int radius) {
		super(name);
		this.radius=radius;
		
	}
	float calculateArea() {
		return (float) (3.14*this.radius*this.radius);
	}
	public int getRadius() {
		return radius;
	}
	public void setRadius(int radius) {
		this.radius = radius;
	}
}
class Square extends Shape{
	
	private int side;
	Square(String name,int side) {
		super(name);
		this.side=side;
		
	}

	public int getSide() {
		return side;
	}

	public void setSide(int side) {
		this.side = side;
	}

	float calculateArea() {
		return this.side*this.side;
	}
	
}
class Rectangle extends Shape{
	Rectangle(String name,int length,int breadth) {
		super(name);
		this.length=length;
		this.breadth=breadth;
		
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public int getBreadth() {
		return breadth;
	}
	public void setBreadth(int breadth) {
		this.breadth = breadth;
	}
	private int length;
	private int breadth;

	float calculateArea() {
		return this.length*this.breadth;
	}

}