package com.dayfour;

class A {
	int a = 100;
	public void display() {
	System.out.printf("a in A = %d\n", a);
}
}

class B extends A{
	int b = 250;
	public void display() {
	System.out.printf("b in B = %d\n", b);
}
}
	

class C extends B{
	int c = 500;
	public void display() {
	System.out.printf("c in C = %d\n", c);
}
}

public class OopExercise {
	static int a = 555; 
	public static void main(String[] args) { 
		A objA = new A(); 
		B objB1 = new B(); 
		A objB2 = new B(); 
		C objC1 = new C(); 
		B objC2 = new C(); 
		A objC3 = new C(); 
		objA.display(); 
		objB1.display(); 
		objB2.display(); 
		objC1.display(); 
		objC2.display(); 
		objC3.display(); 
		} 
	} 

