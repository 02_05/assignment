package com.dayfour;

import java.util.Scanner;

public class MainShape {
	public static void main(String[] args) {
		int rad;
		int side;
		int len;
		int breadth;
		Scanner sc=new Scanner(System.in);
		System.out.println("1.Circle\n2.Square\n3.Rectangle");
		String str="";
		System.out.println("Enter the shape: ");
		str=sc.nextLine();
		if(str.equalsIgnoreCase("circle")) {
			System.out.println("Enter the radius: ");
			rad=sc.nextInt();
			Circle circle=new Circle("circle",rad);
			System.out.println(circle.calculateArea());
		}else if(str.equalsIgnoreCase("square")) {
			System.out.println("Enter the side: ");
			side=sc.nextInt();
			Square square=new Square("square",side);
			System.out.println(square.calculateArea());
		}else if(str.equalsIgnoreCase("rectangle")){
			System.out.println("Enter the length and breadth: ");
			len=sc.nextInt();
			breadth=sc.nextInt();
			Rectangle rectangle=new Rectangle("Rectangle",len,breadth);
			System.out.println(rectangle.calculateArea());
			
		}else {
			System.out.println("select any one from above");
		}
		sc.close();
	}
}
