package com.dayfour;

public abstract class Card {
	protected String holderName;
	protected String cardNumber;
	protected String expiryDate;
	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	Card(String holderName,String cardNumber,String expiryDate){
		this.holderName=holderName;
		this.cardNumber=cardNumber;
		this.expiryDate=expiryDate;
	}
}
	
class MembershipCard extends Card{
	private int rating;

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
	MembershipCard(String holderName, String cardNumber, String expiryDate,int ratring) {
		super(holderName, cardNumber, expiryDate);
		this.rating=ratring;
		
	}

	
}

class PaybackCard extends Card{
	PaybackCard(String holderName, String cardNumber, String expiryDate,int pointsEarned,double totalAmount) {
		super(holderName, cardNumber, expiryDate);
		this.pointsEarned=pointsEarned;
		this.totalAmount=totalAmount;
		
	}
	private int pointsEarned;
	private double totalAmount;
	public int getPointsEarned() {
		return pointsEarned;
	}
	public void setPointsEarned(int pointsEarned) {
		this.pointsEarned = pointsEarned;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	

}


