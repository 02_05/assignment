package com.dayfour;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		String str="";
		int points;
		double amount;
		Scanner sc=new Scanner(System.in);
		System.out.println("Select the card\n1.Payback Card\n2.Membership Card");
		int select=sc.nextInt();
		if(select==1) {
			System.out.println("Enter the card details: ");
			str=sc.next();
			System.out.println("Enter points in card: ");
			points=sc.nextInt();
			System.out.println("Enter amount: ");
			amount=sc.nextDouble();
			String[] ch=str.split("\\|");
			PaybackCard pay = new PaybackCard(ch[0],ch[1],ch[2],points,amount);
			System.out.println(pay.holderName+ " Payback Card details: ");
			System.out.println("Card Number "+ pay.cardNumber);
			System.out.println("Points Earned "+pay.getPointsEarned());
			System.out.println("Total Amount "+ pay.getTotalAmount());
		}else if(select==2) {
			System.out.println("Enter the card details: ");
			str=sc.next();
			System.out.println("Enter Ratings: ");
			int ratings=sc.nextInt();
			String[] ch=str.split("\\|");
			MembershipCard mem = new MembershipCard(ch[0],ch[1],ch[2],ratings);
			System.out.println(mem.holderName+ " Membership Card details: ");
			System.out.println("Card Number "+ mem.cardNumber);
			System.out.println("Ratings "+mem.getRating());
		}else {
			System.out.println("Select any one from above(1 or 2)!...");
		}
		sc.close();
		}
}
