package com.daythree;

import java.util.Scanner;

public class AscendingOrder {
	public static void main(String[] args) {
		int[] arr=new int[10];
		int i;
		Scanner sc=new Scanner(System.in);
		for(i=0;i<10;i++) {
			arr[i]=sc.nextInt();
		}
		for(i=0;i<9;i++) {
			for(int j=i+1;j<10;j++) {
				if(arr[j]<arr[i]) {
					arr[i]=arr[i]+arr[j];
					arr[j]=arr[i]-arr[j];
					arr[i]=arr[i]-arr[j];
				}
			}
		}
		for(i=0;i<10;i++) {
			System.out.println(arr[i]);
		}
		sc.close();
	}
}
