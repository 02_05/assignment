package com.daythree;

import java.util.Scanner;

public class SearchElement {
	public static void main(String[] args) {
		int n;
		int i;
		int arr[]= {1,2,3,4,5,6,7,8,9,10};
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number: ");
		n=sc.nextInt();
		for(i=0;i<10;i++) {
			if(n==arr[i]) {
				break;
			}
		}
		if(i==10) {
			System.out.println("The number " + n + " is not present in the array");
		}else {
			System.out.println("The number " + n + " is present in the array");
		}
		sc.close();
	}
}
