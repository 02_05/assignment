package com.daythree;

import java.util.Scanner;

public class PalindromeCheck {
	public static void main(String[] args) {
		String str;
		int i;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the string: ");
		str=sc.nextLine();
		for(i=0;i<str.length()/2;i++) {
			if(str.charAt(i)!=str.charAt(str.length()-(i+1))) {
				break;
			}
		}
		if(i==str.length()/2) {
			System.out.println(str + " is a palindrome");
		}else {
			System.out.println(str + " is not a palindrome");
		}
		sc.close();
	}
}
