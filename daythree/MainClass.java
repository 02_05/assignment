package com.daythree;

import java.util.Scanner;
class UserMainCodee{
	public static String reShape(String str,char ch) {
		String strnew="";
		for(int i=str.length()-1;i>=0;i--) {
			if(i==0) {
				strnew=strnew+str.charAt(0);
			}else {
				strnew=strnew+str.charAt(i)+ch;
			}
		}
		return strnew;
	}
}
public class MainClass {
	public static void main(String[] args) {
		String str;
		char ch;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the string: ");
		str=sc.nextLine();
		System.out.println("Enter the character: ");
		ch=sc.next().charAt(0);
		System.out.println(UserMainCodee.reShape(str, ch));
		sc.close();
	}
}