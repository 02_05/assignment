package com.daythree;

import java.util.Scanner;

public class SubString {
	public static void main(String[] args) {
		int start;
		int end;
		String str;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the string: ");
		str=sc.nextLine();
		System.out.println("Enter the two numbers: ");
		start=sc.nextInt();
		end=sc.nextInt();
		System.out.println(str.substring(start, end));
		sc.close();
	}
}
