package com.daythree;

import java.util.Scanner;

class UserMainCode{
	public static String getString(String str) {
		if(str.charAt(0)=='k' && str.charAt(1)=='b') {
			return (str);
		}else if(str.charAt(0)!='k' && str.charAt(1)=='b') {
			return(str.substring(1));
		}else if(str.charAt(0)=='k' && str.charAt(1)!='b') {
			return("k"+str.substring(2));
		}else {
			return(str.substring(2));
		}
	}
}
public class RemoveTwoChar {
	public static void main(String[] args) {
		String str;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the string: ");
		str=sc.nextLine();
		System.out.println(UserMainCode.getString(str));
		sc.close();
	}
}
