package com.petshop;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.model.PetModelClass;
import com.service.PetServiceImpl;
import com.service.PetServiceIntf;


@WebServlet("/PetAdd")
public class PetAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
    	String name=request.getParameter("name");
		int age=Integer.parseInt(request.getParameter("age"));
		String place=request.getParameter("place");
		PetModelClass pet=new PetModelClass(0,name,age,place);
		PetServiceIntf service=new PetServiceImpl();
		int row=service.add(pet);
		if(row>0) {
			out.println("<h3 style='text-align:center;'>Successfuly added</h3>");
			RequestDispatcher dispatch=request.getRequestDispatcher("addPet.jsp");
			dispatch.include(request,response);
			//response.sendRedirect("addPet.jsp");
		}
		
	}

}
