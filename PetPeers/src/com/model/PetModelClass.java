package com.model;

public class PetModelClass {
	private int pet_id;
	private String name;
	private int age;
	private String place;
	public PetModelClass(int pet_id, String name, int age, String place) {
		super();
		this.pet_id = pet_id;
		this.name = name;
		this.age = age;
		this.place = place;
	}
	public int getPet_id() {
		return pet_id;
	}
	public void setPet_id(int pet_id) {
		this.pet_id = pet_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	
	
}
