package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.dbconnection.DBConnection;
import com.model.PetModelClass;

public class PetDaoImpl implements PetDaoIntf{
	Connection con=DBConnection.getConnection();
	public int addPet(PetModelClass pet) {
		int row=0;
		try{
			con=DBConnection.getConnection();
			PreparedStatement ps=con.prepareStatement("insert into pets(petname,petage,petplace) values(?,?,?)");
			ps.setString(1,pet.getName());
			ps.setInt(2,pet.getAge());
			ps.setString(3,pet.getPlace());
			row=ps.executeUpdate();
		}catch(Exception e){
			 System.out.println(e);
		}
		return row;
	}

}
