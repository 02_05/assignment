<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Pet</title>
<style>
.divtag{
position:relative; 
top:70px;
background-color:black;
padding:0.5%

}
.log{
position:fixed;
right:25px;
color:white;
}
.col{
color: white
}
a{
text-decoration: none;
}
table{
width:30%;
border:1px;
border-collapse:collapse; 
margin-top:10%;
margin-left:auto;
margin-right:auto;

}
caption{
font-size:large;
text-align: left;
background-color:#929b94;
}
th,td{
padding:15px;
}
.type{
	/* width:50%;  */
	padding:7px 50px;
}
</style>
</head>
<body>
<div class="divtag">
<a href="#" class="col">PetShop</a>
<a href="homePage.jsp" class="col">Home</a>
<a href="myPets.jsp" class="col">MyPets</a>
<a href="addPet.jsp" class="col">AddPet</a>
<a href="logout.jsp" class="log">Logout</a>
</div>
<form action="PetAdd" method="post">
<table border=1>
<caption>Pet Information</caption>
<tr>
<td>Name</td>
<td><input type="text" name="name" class="type" required></td>
</tr>
<tr>
<td>Age</td>
<td><input type="text" name="age" class="type" required></td>
</tr>
<tr>
<td>Place</td>
<td><input type="text" name="place" class="type" required></td>
</tr>
<tr>
<td colspan="2"><input type="submit" value="Add Pet">
<input type="reset" value="Cancel"></td>
</tr>
</table>
</form>
</body>
</html>