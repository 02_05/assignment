<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login Page</title>
<style>
.divtag{
position:relative; 
top:50px;
background-color:black;
padding:0.5%
}
.log{
position:fixed;
right:25px;
color:white;
}
.col{
color: white
}
a{
text-decoration: none;
}

table{
margin-top:7%;
margin-left:auto;
margin-right:auto; 
} 
h2{
text-align: center;
}
</style>
</head>
<body>
<h2>Welcome To Pet Peers</h2>
<div class="divtag">
<a href="#" class="col">PetShop</a>
<a href="registrationPage.jsp" class="log">Sign Up</a>
</div>
<form action="LoginServlet">
<table>
<tr><td><label for="name">UserName</label></td></tr>
<tr><td></td></tr>
<tr><td><input type="text" name="name" placeholder="UserName" required></td></tr>
<tr><td><label for="pass">Password</label></td></tr>
<tr><td><input type="password" name="pass" placeholder="Password" required></td></tr>
<tr><td><input type="submit" value="Login"/></td></tr>
</table>
</form> 
</body>
</html>
