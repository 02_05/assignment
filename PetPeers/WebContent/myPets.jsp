<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.sql.*" import="com.dbconnection.DBConnection"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>My Pets</title>
<style>
.divtag{
position:relative; 
top:70px;
background-color:black;
padding:0.5%

}
.log{
position:fixed;
right:25px;
color:white;
}
.col{
color: white
}
a{
text-decoration: none;
}
table{
width:40%;
border:2px solid black;
border-collapse:collapse;
margin-top:10%;
margin-left:auto;
margin-right:auto;
}
</style>
</head>
<body>
<div class="divtag">
<a href="#" class="col">PetShop</a>
<a href="homePage.jsp" class="col">Home</a>
<a href="myPets.jsp" class="col">MyPets</a>
<a href="addPet.jsp" class="col">AddPet</a>
<a href="logout.jsp" class="log">Logout</a>
</div>
<%
try{
	int id=Integer.parseInt(session.getAttribute("id").toString());
	Connection con=DBConnection.getConnection(); 
	PreparedStatement ps=con.prepareStatement("select * from pets where owner_id=?");     
	ps.setInt(1,id);
	ResultSet rs=ps.executeQuery();
	if(!rs.next()){
		out.println("<table><tr><td style='text-align:center'>");
		out.println("No pets are available in your cart"+"</td></td></table>");
	}else{
		out.println("<table border=1>"+
				"<caption>Pet List</caption>"+
				"<tr>"+
				"<th>Pet Id</th>"+
				"<th>Pet Name</th>"+
				"<th>Pet Age</th>"+
				"<th>Pet Place</th>"+
				"</tr>");
		do{
			out.println("<tr><td>"+rs.getInt("pet_id")+"</td><td>"+rs.getString("petname")+"</td><td>"+rs.getInt("petage")+"</td><td>"+rs.getString("petplace")+"</td></tr>");
		}while(rs.next());
		out.println("</table>");
	}
}catch(Exception e){
	
}
%>

</body>
</html>