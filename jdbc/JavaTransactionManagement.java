/*Jdbc Transaction Management
1.Create a table named customer including name,salary,email.
2.One of the constraints on the table is that email has to be unique. If we enter same email a second time to violate this constraint. It results in SQL exception. Have to  rollback transaction programmatically in exception handling block.
Sample Input and Output:
If Email is unique then data should save successfully.
Enter name
revathi
Enter salary
10000
Enter email
a@hcl.com
Want to add more records y/n
y
Enter name
drishnaa
Enter salary
20000
enter email
b@hcl.com
Want to add more records y/n
n
Data Saved Successfully
If Email is  violated.
Enter name
revathi
Enter salary
10000
Enter email
a@hcl.com
Email Id must be unique . Data Rollback successfully.
*/


package com.jdbc;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class JavaTransactionManagement {
	static Connection con=null;
	public static void main(String[] args) throws Exception {
		try {
			Scanner sc=new Scanner(System.in);
			char input;
			Class.forName("com.mysql.cj.jdbc.Driver");//registering the driver
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/practice","root","root");//establishing connection
			con.setAutoCommit(false); //to perform rollback here making setAutoCommit=false
			DatabaseMetaData dbm=con.getMetaData();
			ResultSet table=dbm.getTables(null, null, "customer", null);
			if(!table.next()) {
				Statement st=con.createStatement(); //statement creation
				st.execute("create table customer (name varchar(25),salary float(10,2),email varchar(30) unique)"); //query to create table
				System.out.println("Table Created");
			}
			do {
				System.out.println("Enter name: ");
				String name=sc.next();
				System.out.println("Enter salary: ");
				float salary=sc.nextFloat();
				System.out.println("Enter Email: ");
				String email=sc.next();
				PreparedStatement ps=con.prepareStatement("insert into customer values(?,?,?)"); //query to insert values into the table using prepared statement
				ps.setString(1, name);
				ps.setFloat(2, salary);
				ps.setString(3, email);
				int row=ps.executeUpdate();
				con.commit(); //performing commit
				System.out.println(row + " rows inserted");
				System.out.println("Want to add more records y/n");
				input=sc.next().charAt(0);
			}while(input!='n');
			if(input=='n') {
				System.out.println("Data Saved Successfully");
			}
			sc.close();
		} catch (Exception e) {
			con.rollback(); //performing rollback
			System.out.println("Email Id must be unique . Data Rollback successfully.");
			
		}
		
	}

}
