//4. Write a code to delete a record whose username field contains �steve�.

package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class JdbcDelete {
	public static void main(String[] args) throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver"); //registering driver class
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/practice","root","Mysql@root1"); //creating connection
		PreparedStatement ps=con.prepareStatement("delete from Users where username='steve'"); //creating statement and establishing connection
		int row=ps.executeUpdate(); //query execution
		System.out.println(row + " row deleted");
		con.close(); //closing connection
	}
	
}
