//3.�Write a code to update all the details� of �steve paul�.

package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class JdbcUpdate {
	public static void main(String[] args) throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver"); //registering driver class
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/practice","root","Mysql@root1"); //connection creation
		PreparedStatement ps=con.prepareStatement("update users set username='john',password='abcde',email='john@hcl.com' where fullname='steve paul'");
		int row=ps.executeUpdate(); //query execution
		System.out.println(row + " row updated");
		con.close(); //closing connection
	}

}
