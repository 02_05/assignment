/*1.Let�s write code to insert a new record into the table Users with following details:
username: steve
password: secretpass
fullname: steve paul
email: steve.paul@hcl.com
*/

package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class JdbcInsert {
	public static void main(String[] args) throws Exception {		
		Class.forName("com.mysql.cj.jdbc.Driver"); //register the driver class
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/practice","root","Mysql@root1"); //connection creation
		Statement st=con.createStatement(); //statement creation
		int row=st.executeUpdate("insert into Users values('steve','secretpass','steve paul','steve.paul@hcl.com')"); //query execution
		if(row!=0) {
			System.out.println(row + " row inserted");
		}
		con.close(); //closing connection
	}

}
