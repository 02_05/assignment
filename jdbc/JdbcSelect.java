//2. Select all records from the Users table and print out details for each record

package com.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class JdbcSelect {
	public static void main(String[] args) throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver"); //registering the driver class
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/practice","root","Mysql@root1"); //connection creation
		PreparedStatement ps=con.prepareStatement("select * from User"); //statement creation
		ResultSet result=ps.executeQuery(); //query execution
		while(result.next()) {
			System.out.println(result.getString("username")+ " " + result.getString("password")+ " "+result.getString("fullname")+ " "+result.getString("email"));
		}
		con.close(); //closing connection
	}

}
