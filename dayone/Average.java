package com.dayone;

import java.util.Scanner;

public class Average {
	
	int calculateAverage(int a,int b, int c) {
		return (a+b+c)/3;
	}
	public static void main(String[] args) {
		int num1;
		int num2;
		int num3;
		Average avg = new Average();
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter three numbers: ");
		num1=sc.nextInt();
		num2=sc.nextInt();
		num3=sc.nextInt();
	    System.out.println(avg.calculateAverage(num1, num2, num3));
	    sc.close();
	}
}
