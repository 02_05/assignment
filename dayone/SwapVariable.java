package com.dayone;

public class SwapVariable {

	public static void main(String[] args) {
		int a=2;
		int b=5;
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("The value of a= "+ a);
		System.out.println("The value of b= "+ b);
	}

}
