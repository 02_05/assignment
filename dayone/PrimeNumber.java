package com.dayone;

import java.util.Scanner;

public class PrimeNumber {
	    public static void main(String []args){
	       int p;
	       Scanner sc=new Scanner(System.in);
	       System.out.println("Enter the number: ");
	       p=sc.nextInt();
	       int i;
	       for(i=2;i<=p/2;i++){
	           if(p%i==0){
	               break;
	           }
	       }
	       if(i>p/2){
	           System.out.println(p + " is a prime number ");
	       }else{
	           System.out.println(p + " is not a prime number ");
	       }
	    sc.close(); 
	    }
}
