package com.dayone;

import java.util.Scanner;

class UserMainCodee{
	public static String getLargestWord(String str) {
		int len=0;
		String word="";
		String[] arr=str.split("\\s");
		for(String s:arr) {
			if(len<s.length()) {
				len=s.length();
				word=s;
			}
		}
		return word;
	}
}

public class ProgramTen {
	public static void main(String[] args) {
		String str;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String: ");
		str=sc.nextLine();
		System.out.println(UserMainCodee.getLargestWord(str));
		sc.close();
	}
}
