package com.dayone;

import java.util.Scanner;

public class AsciiValue {
	   public static void main(String[] args) {
	       char ch;
	       Scanner sc=new Scanner(System.in);
	       System.out.println("Enter the character: ");
	       ch=sc.next().charAt(0);
	       int c=ch;
	       System.out.println("The ASCII value of given character is:" + c);
	       sc.close();
	   }
}
