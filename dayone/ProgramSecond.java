package com.dayone;

import java.util.Scanner;

public class ProgramSecond {
	public static void main(String[] args) {
		int num;
		int num1;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the first number: ");
		num=sc.nextInt();
		System.out.println("Enter the second number: ");
		num1=sc.nextInt();
		System.out.println("Addition: " + (num+num1));
		System.out.println("Multiply: " + (num*num1));
		System.out.println("Subtract: " + (num-num1));
		System.out.println("Divide: " + (num/num1));
		System.out.println("Remainder: " + (num%num1));
		sc.close();
	}
}
