package com.dayone;

import java.util.Scanner;

class OddCheck{
	public static int checkSum(int num) {
		int temp=num;
		int count = 0;
		while(temp!=0) {
			if((temp%10)%2!=0) {
				count++;
			}
			temp=temp/10;
		}
		if(count%2!=0) {
			return 1;
		}else {
			return -1;
		}
	}
}

public class ProgramEight {
	public static void main(String[] args) {
		int num;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number: ");
		num=sc.nextInt();
		int result=OddCheck.checkSum(num);
		if(result==1) {
			System.out.println("Sum of odd digits is odd");
		}else {
			System.out.println("Sum of odd digits is even");
		}
		sc.close();
	}
}
