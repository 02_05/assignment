package com.dayone;

import java.util.Scanner;

class UserMainCode{
	public static int SumOfSquaresOfEvenDigits(int num) {
		int temp=num;
		int sum=0,r;
		while(temp!=0) {
			r=temp%10;
			if(r%2==0) {
				sum=sum+(r*r);
			}
			temp=temp/10;
		}
		return sum;
	}
}
public class ProgramNine {
	public static void main(String[] args) {
		int num;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number: ");
		num=sc.nextInt();
		System.out.println(UserMainCode.SumOfSquaresOfEvenDigits(num));
		sc.close();
	}
}
