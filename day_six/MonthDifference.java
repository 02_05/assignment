/*Given a method with two date strings in yyyy-mm-dd format as input. Write code to find the difference between two dates in months.Include a class UserMainCode with a static method getMonthDifference which accepts two date strings as input.The return type of the output is an integer which returns the diffenece between two dates in months.Create a class Main which would get the input and call the static method getMonthDifference?present in the UserMainCode. 
*/
package com.daysix;

import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;

class MonthDifferenceDemo{
	static int getMonthDifference(String date1,String date2) {
		Period diff=Period.between(LocalDate.parse(date1), LocalDate.parse(date2));
		return diff.getMonths()+(12*diff.getYears());
}
}
public class MonthDifference {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the date 1: ");
		String date1=sc.next();
		System.out.println("Enter the date 2: ");
		String date2=sc.next();
		System.out.println("Difference between the given months: ");
		System.out.println(MonthDifferenceDemo.getMonthDifference(date1, date2));
		sc.close();
	}
}
