/*.Given a date string in the format�dd/mm/yyyy, write a program to convert the given date to the format�dd-mm-yy.�Include a class UserMainCode with a static method �convertDateFormat� that accepts a String and returns a String.Create a class Main which would get a String as input and call the static method convertDateFormat present in the�UserMainCode.�
*/
package com.daysix;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

class UserMainCodeDemo{
	static String ConvertDateFormat(String date) {
		String st="";
		try {
			Date d=new SimpleDateFormat("dd/MM/yyyy").parse(date);
			st=new SimpleDateFormat("dd-MM-yy").format(d);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return st;
	}
}

public class DateFormatDemo {
	public static void main(String[] args) {
		String strDate;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the string Date: ");
		strDate=sc.next();
		System.out.println(UserMainCodeDemo.ConvertDateFormat(strDate));
		sc.close();
	}
}
