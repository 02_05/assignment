/*Write a code to read?two int array lists of size 5 each as input and to merge the two arrayLists, sort the merged arraylist in ascending order and fetch the elements at 2nd, 6th and 8th index into a new arrayList and return the final ArrayList.Include a class UserMainCode with a static method sortMergedArrayList which accepts 2 ArrayLists. The return type is an ArrayList with elements from 2,6 and 8th index position .Array index starts from position 0. Create a Main class which gets two array list of size 5 as input and call the static method sortMergedArrayList present in the UserMainCode. 
*/
package com.daysix;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

class ArrayListDemo{
	static List<Integer> sortMergedArrayList(List<Integer> l1,List<Integer> l2) {
		List<Integer> lst3=new ArrayList<>(10);
		lst3.addAll(l1);
		lst3.addAll(l2);
		Collections.sort(lst3);
		List<Integer> fl=new ArrayList<>();
		fl.add(lst3.get(2));
		fl.add(lst3.get(6));
		fl.add(lst3.get(8));
		return fl;
	}
}
public class MainClass {
	public static void main(String[] args) {
		List<Integer> lst1=new ArrayList<>(5);
		List<Integer> lst2=new ArrayList<>(5);
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the First List: ");
		int j=0;
		while(j<5) {
			int i=sc.nextInt();
			lst1.add(i);
			j++;
		}
		System.out.println("Enter the Second List: ");
		j=0;
		while(j<5) {
			int i=sc.nextInt();
			lst2.add(i);
			j++;
		}
		System.out.println("Elements in 2,6 & 8th index after sorting: " +ArrayListDemo.sortMergedArrayList(lst1, lst2));
		sc.close();
		
		
	}
}
