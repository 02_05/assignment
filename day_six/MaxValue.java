//Write a Java program to get the maximum value of the year, month, week, date from the current date of a default calendar 

package com.daysix;

import java.util.Calendar;

public class MaxValue {
	public static void main(String[] args) {
		Calendar cal=Calendar.getInstance();
		System.out.println("Maximum Year :" + cal.getActualMaximum(Calendar.YEAR));
		System.out.println("Maximum Month :" + cal.getActualMaximum(Calendar.MONTH));
		System.out.println("Maximum Week :" + cal.getActualMaximum(Calendar.WEEK_OF_YEAR));
		System.out.println("Maximum Date :" + cal.getActualMaximum(Calendar.DATE));
	}
}
