/* Write a program to read a string  and to test whether first and last character are same. 
 * The string is said to be be valid if the 1st and last character are the same. 
 * Else the string is said to be invalid.
 * Include a class UserMainCode with a static method checkCharacters which accepts a string as input .
 * The return type of this method is an int.  Output should be 1 if the first character and 
 * last character are same . 
 * If they are different then return -1 as output.
 * Create a class Main which would get the input as a string and 
 * call the static method checkCharacterspresent in the UserMainCode.
*/

package com.daysix;

import java.util.Scanner;

class UserMainCodeCheckChar{
	static int checkCharacters(String str){
		if(str.charAt(0)==str.charAt(str.length()-1)) {
			return 1;
		}else {
			return -1;
		}
		
	}
}
public class MainCheckCharacters {
	public static void main(String[] args) {
		String str;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the string: ");
		str=sc.nextLine();
		int result=UserMainCodeCheckChar.checkCharacters(str);
		if(result==1) {
			System.out.println("Valid");
		}else {
			System.out.println("Invalid");
		}
		sc.close();
	}
}
