/*Write a program to read a string of even length and 
 *to fetch two middle most characters from the input string and return it as string output.
 *Include a class UserMainCode with a static method getMiddleChars 
 *which accepts a string of even length as input . 
 *The return type is a string which should be the middle characters of the string.
 *Create a class Main which would get the input as a string and
 *call the static method getMiddleCharspresent in the UserMainCode.
*/

package com.daysix;

import java.util.Scanner;

class UserMainCodeNext{
	static String getMiddleChars(String str){
		return str.substring((str.length()/2)-1, (str.length()/2)+1);
	}
}
public class MainNext {
	public static void main(String[] args) {
		String str;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the string: ");
		str=sc.next();
		System.out.println("Middle characters in a string: " + UserMainCodeNext.getMiddleChars(str));
		sc.close();
	}
}
