/*A Company wants to obtain employees of a particular designation. 
 *You have been assigned as the programmer to build this package. 
 *You would like to showcase your skills by creating a quick prototype. 
 *The prototype consists of the following steps: 
 *Read Employee details from the User. The details would include name and designaton in the given order. 
 *The datatype for name and designation is string. 
 *Build a hashmap which contains the name as key and designation as value. 
 *You decide to write a function obtainDesignation which takes the hashmap and 
 *designation as input and returns a string array of employee names who belong to that designation as output. 
 *Include this function in class UserMainCode. 
 *Create a Class Main which would be used to read employee details in step 1 and build the hashmap. 
 *Call the static method present in UserMainCode.
 */

package com.daysix;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Scanner;

class ObtainDetails{
	static String[] obtainDesignation(HashMap<String,String> hmap, String designation) {
		int i = 0;
		Iterator<Entry<String, String>> it=hmap.entrySet().iterator();
		String[] arr=new String[hmap.size()];
		while (it.hasNext() ) {
			String key=it.next().getKey();
			String value=hmap.get(key);
			if(value.equals(designation)) {
				arr[i]=key;
				i++;
			}
		}
		return arr;
		
	}
}

public class EmployeeDetails {
	public static void main(String[] args) {
		String name;
		String designation;
		HashMap<String, String> map=new HashMap<>();
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the num");
		int n=sc.nextInt();
		System.out.println("Enter the name and designation: ");
		for(int i=0;i<n;i++) {
			name=sc.next();
			designation=sc.next();
			map.put(name, designation);
		}
		System.out.println("Enter the designation: ");
		String des=sc.next();
		String[] str=(ObtainDetails.obtainDesignation(map, des));
		System.out.println("Employees with the given designation: ");
		for(int i=0;i<str.length;i++) {
			if(str[i]!=null)
				System.out.println(str[i]);	
		}
		sc.close();
	}
}
