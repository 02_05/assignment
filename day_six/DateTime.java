//Write a java program to print current date and time in the specified format

package com.daysix;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateTime {
	public static void main(String[] args) {
		//before java 1.8
		Date d=new Date();
		SimpleDateFormat date=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println(date.format(d));
		
		//using the class from java.time package (java 1.8 features)
		LocalDateTime dt=LocalDateTime.now();
		DateTimeFormatter datetime=DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		System.out.println(dt.format(datetime));
		
	}
}
