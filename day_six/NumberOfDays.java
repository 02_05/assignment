/*Include a class�UserMainCode�with a static method �getNumberOfDays� 
 * that accepts 2 integers as arguments and returns an integer. 
 * The first argument corresponds to the year and the second argument corresponds to the month code. 
 * The method returns an integer corresponding to the number of days in the month.�
 * Create a class Main which would get 2 integers as input and 
 * call the static method�getNumberOfDays�present in the�UserMainCode.�
*/

package com.daysix;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

class CalculateNoOfDays{
	static int getNumberOfDays(int year,int month) {
		Calendar cal=new GregorianCalendar(year,month,1);
		int days=cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		return days;
	}
}
public class NumberOfDays {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Year: ");
		int year=sc.nextInt();
		System.out.println("Enter the Month: ");
		int month=sc.nextInt();
		System.out.println("Number of days in a given month: ");
		System.out.println(CalculateNoOfDays.getNumberOfDays(year,month));
		sc.close();
	}

}
