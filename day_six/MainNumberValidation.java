/*Write a program to read a string of 10 digit number , 
 * check whether the string contains a 10 digit number in the format XXX-XXX-XXXX where 'X' is a digit.
 * Include a class UserMainCode with a static method validateNumber which accepts a string as input .
 * The return type of the output should be 1 if the string meets the above specified format . 
 * In case the number does not meet the specified format then return -1 as output.
 * Create a class Main which would get the input as a String of numbers and 
 * call the static methodvalidateNumber present in the UserMainCode.
*/
package com.daysix;

import java.util.Scanner;

class UserMainCodeNumValidation{
	static int validateNumber(String stringNumber) {
		int i;
		for(i=0;i<stringNumber.length();i++) {
			if(i==3 || i==7) {
				if(stringNumber.charAt(i)!='-') {
					break;
				}
			}else {
				if(!(stringNumber.charAt(i)>='0' && stringNumber.charAt(i)<='9')) {
					break;
				}
			}
		}
		
		if(i==stringNumber.length() && stringNumber.length()==12) {
			return 1;
		}else {
			return -1;
		}
	}
}
public class MainNumberValidation {
	public static void main(String[] args) {
		String str;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the string: ");
		str=sc.nextLine();
		int result=UserMainCodeNumValidation.validateNumber(str);
		if(result==1) {
			System.out.println("Valid");
		}else {
			System.out.println("Invalid Number Format");
		}
		sc.close();
	}
}
