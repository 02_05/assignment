/*Write a program to read  two String variables in DD-MM-YYYY.Compare the two dates and 
 *return the older date in 'MM/DD/YYYY' format. 
 *Include a class UserMainCode with a static method findOldDate which accepts the string values. 
 *The return type is the string. 
 *Create a Class Main which would be used to accept the two string values and 
 *call the static method present in UserMainCode. 
 *Sample Input 1: 
  05-12-1987 
  08-11-2010 
 *Sample Output 1: 
  12/05/1987 
*/


package com.daysix;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

class OlderDateDemo{
	static String findOldDate(String date1,String date2) throws ParseException {
		String str;
		Date d1=new SimpleDateFormat("dd-MM-yyyy").parse(date1);
		Date d2=new SimpleDateFormat("dd-MM-yyyy").parse(date2);
		int diff=d1.compareTo(d2);
        if(diff==0) {
        	str="Both dates are equal";
        }else if(diff>0) {
        	str=new SimpleDateFormat("MM/dd/yyyy").format(d2);
        }else {
        	str=new SimpleDateFormat("MM/dd/yyyy").format(d1);
  
        }
		return str;
	}
}

public class OlderDate {
	public static void main(String[] args) throws ParseException {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter te first date: ");
		String firstDate=sc.next();
		System.out.println("Enter the second date: ");
		String secondDate=sc.next();
		System.out.println("Older date is: ");
		System.out.println(OlderDateDemo.findOldDate(firstDate, secondDate));
		sc.close();
	}
}
