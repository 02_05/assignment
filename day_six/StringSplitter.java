package com.daysix;

import java.util.Scanner;

class StringSplitterDemo{
	static String[] manipulateLiteral(String str,char delimiter) {
		String[] sortedArray=str.split("\\" + delimiter + "");
		for(int i=0;i<sortedArray.length;i++) {
			char[] ch=sortedArray[i].toCharArray();
			sortedArray[i]="";
			for(int j=ch.length-1;j>=0;j--) {
				sortedArray[i]=sortedArray[i]+ch[j];
			}
			sortedArray[i]=sortedArray[i].toLowerCase();
		}
		return sortedArray;	
	}
}
public class StringSplitter {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the String: ");
		String st=sc.next();
		System.out.println("Enter the delimiter");
		char del=sc.next().charAt(0);
		String[] sorted=StringSplitterDemo.manipulateLiteral(st,del);
		for(int i=0;i<sorted.length;i++) {
			System.out.println(sorted[i]);
		}
		sc.close();
	}
}
