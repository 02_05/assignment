package com.carinventory;

import java.sql.Connection;
import java.sql.DriverManager;


public class DBConnection {
	public static Connection getConnection() throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver"); //registering driver class
		Connection con=DriverManager.getConnection ("jdbc:mysql://localhost:3306/car_inventory","root","root"); //connection creation
		return con; //returning the connection object
	}

}
