package com.carinventory;

import java.util.Scanner;

public class CarInventory {
	public static void main(String[] args) throws Exception {
		CarCrudOperations car=new CarCrudOperations (); //creating object for crud operation
		String input;
		Scanner sc=new Scanner(System.in);
		System.out.println("welcome to Mullet Joe's Gently Used Autos!...");
		do {
			System.out.println("1.Add\n2.List\n3.Delete\n4.Update\n5.Quit");
			System.out.println("Enter the Command: ");
			input=sc.next();
			if(input.equalsIgnoreCase("add") || input.equals("1")){
				car.insertMethod();
			}else if (input.equalsIgnoreCase("list")|| input.equals("2")) { 
				car.selectMethod();
			}else if(input.equalsIgnoreCase("delete")||input.equals("3")){
				car.deleteMethod();
			}else if(input.equalsIgnoreCase("update") || input.equals("4")) { 
				car.updateMethod(); 
			}else if (input.equalsIgnoreCase("quit")||input.equals("5")) {
				System.out.println("Thank You!...");

			}else {
				System.out.println("Sorry, but " + input +" is not a valid command. Please try again!..");
			}
		}while(!(input.equalsIgnoreCase("quit")));
		sc.close();
	}

}
