package com.daytwo;

class Room {
	int roomNo;
	String roomType;
	float roomArea;
	boolean acMachine;
	void setData(int no,String type,float area,boolean ac) {
		roomNo=no;
		roomType=type;
		roomArea=area;
		acMachine=ac;
	}
	void displayData() {
		System.out.println("The Room No is: " +roomNo);
		System.out.println("The Room Type is: " +roomType);
		System.out.println("The Room Area is: " +roomArea);
		System.out.println("Need AC : " +acMachine);
	}
}
public class RoomDemo{
	public static void main(String[] args){
		Room room=new Room();
		room.setData(2,"single",550,true);
		room.displayData();
	}
}	
