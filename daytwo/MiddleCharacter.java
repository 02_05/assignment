package com.daytwo;

public class MiddleCharacter {
	public static void main(String[] args) {
		String str="367";
		if((str.length())%2!=0) {
			System.out.println("The middle character in the string: " +str.substring(((str.length()/2)), (str.length()/2)+1));
		}else {
			System.out.println("The middle character in the string: " +str.substring(((str.length()/2)-1), (str.length()/2)+1));
		}
	}
}
