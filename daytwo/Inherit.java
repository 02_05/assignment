package com.daytwo;

class A {
	public void animal() {
		System.out.println("cat");
	}
}
class B extends A{
	public String bird() {
		return "parrot";
	}
	
}
public class Inherit{
	public static void main(String[] args) {
		B b=new B();
		b.animal();
		System.out.println(b.bird());
	}
}